# TYPO3 Extension ``tp3_openhours``
[![Latest Stable Version](https://poser.pugx.org/web-tp3/tp3_openhours/v/stable)](https://packagist.org/packages/web-tp3/tp3_openhours)
[![Daily Downloads](https://poser.pugx.org/web-tp3/tp3_openhours/d/daily)](https://packagist.org/packages/web-tp3/tp3_openhours)
[![Total Downloads](https://poser.pugx.org/web-tp3/tp3_openhours/downloads)](https://packagist.org/packages/web-tp3/tp3_openhours)
[![License](https://poser.pugx.org/web-tp3/tp3_openhours/license)](https://packagist.org/packages/web-tp3/tp3_openhours)


provides Openhours for tt_address

##install
 
The recommended way to install the extension is by using (Composer)
[1]. In your Composer based TYPO3 project root, just do `composer require web-tp3/tp3_openhours`. 

##Manual
[Manual](https://web.tp3.de/manual/tp3_openhours.html)


## Administration corner

### Versions and support

| tp3_openhours | TYPO3      | PHP       | Support/Development                          |
| ------------- | ---------- | ----------|----------------------------------------------|
| 1.x           | 9.5 - 10.4 | 7.2 - 7.4 | ViewHelper returns OpenHours                         |
| 0.x           | 8.7 - 9.5  | 7.0 - 7.2 | Tp3BusinessView Integration, Tp3mods Json-LD |

###viewhelper usage

	<f:for each="{oh:OpenHours(parameters:settings.list.openHours.parameters,addresses:address)}" as="address" iteration="iterator">
		<f:debug>{address}</f:debug>
</f:for>

##Copyleft
This file is part of the "tp3_openhours" Extension for TYPO3 CMS.


[Manual](https://web.tp3.de/manual/tp3_openhours.html)

[Repository](https://bitbucket.org/web-tp3/tp3_openhours)
